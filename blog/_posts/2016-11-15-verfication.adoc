= Putting the Rust in Trust
Corey Richardson
v3 2016-11-15: Incorporated ticki's feedback

After working at Data61 with the verification team, and many conversations with
Gerwin Klein, I have a good plan on how I think I want to achieve the goal of
*Trustworthy Rust*.

== Introduction to formal verification

:bs: https://en.wikipedia.org/wiki/Binary_search_algorithm
:javabs: https://research.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html

Formal verification is using mathematical techniques to analyze software. In
general, one wants to show that a particular program implements some
specification ("functional correctness"). But there are other things you can
show too! For example, seL4 shows high-level security properties like integrity
and confidentiality. Actually doing this is quite hard, but relatively well
understood these days. You need to model the program, then show that the model
satisfies the spec. Hopefully, the model corresponds to reality. In practice,
many verification techniques have serious problems with reality-correspondence.

As one semi-famous example, {bs}[binary search] was "proven correct" many times,
but {javabs}[still had buggy implementations]. What was the failure here? The
model used mathematical integers, but the reality is finite, wrapping words.
This is a complete embarrassment that any principled use of formal methods
should avoid. If your model doesn't correspond to reality, you need a tedious,
manual, error-prone validation of the implementation. We can do better!

== Phase 1: A semantics for Rust

:ecr: ftp://ftp.cs.washington.edu/tr/2015/03/UW-CSE-15-03-02.pdf
:traits: https://open.library.ubc.ca/media/download/pdf/24/1.0220521/4
:ticki: https://ticki.github.io/blog/a-hoare-logic-for-rust/
:rustproof: https://github.com/Rust-Proof/rustproof
:mir: https://blog.rust-lang.org/2016/04/19/MIR.html
:rb: http://plv.mpi-sws.org/rustbelt/

The first phase is modeling the semantics of Rust programs. There are
{ecr}[quite] {traits}[a] {ticki}[few] {rustproof}[partial] approaches to this
so far. This is not to disparage their efforts - I hope they continue and
succeed, because more work in this space can only be good!

On the one hand, you want a semantics so you can prove things like soundness of
the language's type system, or *memory safety* of fragments of the language. On
the other hand, sometimes you want to actually prove properties about specific
programs in the language. The former can sometimes use a simplified model,
depending on the type system and guarantees it wants to make. The latter needs
to capture all details of the language that are relevant to the program under
consideration, and is generally much more formalization-intensive.

I have my own in-progress semantics for {mir}[MIR], that I'm working on between
Robigalia sprints. In general, I think MIR is the best short-term approach for
initial verification of Rust programs. In the long term, someone will need to
write a full semantics for actual-Rust, and write either a verified compiler or
a proof-producing translator from that into MIR. This will be very challenging,
as Rust's type system is rather complex. I suspect the {rb}[RustBelt] folks will
have the best start of this.

At this point, implementations of Rust (eg, rustc) could use the semantics to
resolve confusions and as a ultimate arbiter of deciding correctness of some
program transformation (eg, an optimization). In the long run, the semantics
could serve as the normative specification of the whole language.


== Phase 2: program logics

:hoare: https://en.wikipedia.org/wiki/Hoare_logic
:core: https://doc.rust-lang.org/core/
:isahoare: https://www.isa-afp.org/entries/Abstract-Hoare-Logics.shtml
:ac: http://ts.data61.csiro.au/projects/TS/autocorres/

Given a semantics for MIR, one can define things like {hoare}[Hoare logics] over
MIR programs. The benefit of having a semantics at this point is that the
program logics can be shown _sound_, which is rather important. Soundness of a
program logic means that properties proved in the logic are also true of the
program. Ticki's and RustProof's works, mentioned above, can not be shown sound
without a semantics for MIR.

You really want properties in the program logic to be nicely composable, so that
proving results about, for example, portions of {core}[libcore] can be slotted
easily into proofs about larger programs. At this point, one could start writing
specifications and proving functional correctness of small programs or library
functions.

My plan for this right now is to use a separation logic and a
{isahoare}[semantic Hoare logic] combined with something like {ac}[AutoCorres]
to make verification easier. I hope to lean very heavily on the extensive
libraries and infrastructure that the Data61's TS group has created. It will be
a lot of work getting this working, but the down-the-line productivity benefits
could be huge.

== Phase 3: verified compilation

:cml: http://cakeml.org/
:ve: http://www.cis.upenn.edu/~stevez/vellvm/
:cc: http://compcert.inria.fr/doc/
:ralloc: https://github.com/redox-os/ralloc

Given a proven-correct MIR program, we need some way to actually _execute_ that
program, in a way that the correctness proofs still apply. And ideally, we would
like to retain most of the efficiency of writing in Rust instead of some higher
level language like Haskell.

My plan for this right now is to write a MIR-to-"link:{cml}[CakeML]" translator,
and use CakeML's verified compiler to do all the heavy lifting of compiling to
machine code. At first it would use their garbage collector for memory
allocation. But with a verified allocator, {ralloc}[potentially written in
Rust], the GC should be avoidable.

In the very long run, it'd be good to connect to something like {cc}[CompCert]
or even {ve}[VeLLVM]. These have their own unique challenges. For particular
high-value programs, it might even be worthwhile to do something like seL4 and
prove correspondence of compiler output and source program. I don't know how
feasible this will be, but it's worth investigating.

Once we've written our program, shown that it satisfies the desired
specification, and have an implementation that formally corresponds to that
program, and have proved that the specification has the properties we depend on,
we can start relying on these programs for the most critical of software. At
this point, our software is truly _trustworthy_.

This is my long-term vision, and a lot of sweat and elbow grease needs to go
into making it a reality. But my hope is that the inherent strengths of Rust
will make it easier and cheaper to build trustworthy software, compared to
traditional methods used in C.

_Thanks Alex Elsayed, James McGlashan, Gerwin Klein, and ticki, for early feedback on this post!_
